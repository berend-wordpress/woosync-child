<?php
/**
 * Plugin Name: WooCommerce Product Sync (Child)
 * Plugin URI: https://plugins.comsi.nl/woocommerce-product-sync-between-webshops/
 * Description: WooCommerce product synchronization
 * Version: 1.3.1
 * Author: ComSi
 * Author URI: https://plugins.comsi.nl
 */

require_once("src/WoocommerceProduct.php");
require_once("src/WoocommerceTerm.php");
require_once("src/WoocommerceAttribute.php");
require_once("src/WoocommerceProductPage.php");
require_once("entities/WoocommerceParentObject.php");
require_once("settings/Settings.php");
require_once("Utility.php");
require_once("Setup.php");
require_once("vendor/autoload.php");
require_once("src/ParentApi.php");
require_once("src/WooCommerceOrder.php");

add_action( 'init', array('Setup', 'init' ) );
register_activation_hook(__FILE__, array('Setup', 'activate'));
