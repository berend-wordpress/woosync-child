<?php
/**
 * Add a settings page to the WooCOmmerce menu.
 */
defined('ABSPATH') OR exit;
class Settings {
    protected static $instance;

    /**
     * @return mixed
     */
    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;

        return self::$instance;
    }

    /**
     * Settings constructor.
     */
    public function __construct() {
        register_setting("parent_webshop", "WebshopUrl");
        register_setting("parent_webshop", "ConsumerKey");
        register_setting("parent_webshop", "ConsumerSecret");

        add_action("admin_menu", array("Settings", "AddMenuPage"));
    }

    /**
     * Adds the actual menu page.
     */
    public static function addMenuPage() {
        add_submenu_page("woocommerce", _x("WooCommerce Product Sync", "Admin sub-menu item", "comc"), "WooCommerce Product Sync", "administrator", "parent-webshop", array("Settings", "showSettings"));
    }

    /**
     * Function to show the settings in WordPress.
     */
    public static function showSettings() {
        ?>
        <form method="post" action="options.php">
            <?php
            settings_fields("parent_webshop");
            do_settings_sections("parent_webshop");
            ?>
            <div class="wrap">
                <h1><?php _e('Connect to parent', 'comc'); ?></h1>
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row"><label for="WebshopUrl"><?php _e('Parent webshop URL', 'comc'); ?></th>
                        <td><input type="text" name="WebshopUrl" id="WebshopUrl" class="regular-text" value="<?php echo get_option("WebshopUrl"); ?>" placeholder="https://" /></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="ConsumerKey"><?php _e('API Consumer Key', 'comc'); ?></label></th>
                        <td>
                            <input type="text" name="ConsumerKey" id="ConsumerKey" class="widefat" value="<?php echo get_option("ConsumerKey"); ?>" placeholder="ck_" />
                            <p class="description" id="home-description"><?php _e('WooCommerce API key created on parent webshop', 'comc'); ?></a></p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="ConsumerSecret"><?php _e('API Consumer Secret', 'comc'); ?></label></th>
                        <td>
                            <input type="text" name="ConsumerSecret" id="ConsumerSecret" class="widefat" value="<?php echo get_option("ConsumerSecret"); ?>" placeholder="cs_" />
                            <p class="description" id="home-description"><?php _e('WooCommerce API key created on parent webshop', 'comc'); ?></a></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p class="submit"><?php submit_button(); ?></p>
            </div>
        </form>
        <?php
    }
}