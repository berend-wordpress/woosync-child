<?php
/**
 * A collection of utility functions used by different classes.
 *
 */
defined('ABSPATH') OR exit;
class Utility {
    /**
     * @param $attribute_name
     * @return null|string
     */
    public static function getWooAttrIdByName($attribute_name) {
        global $wpdb;
        $wooAttributeName = str_replace("pa_", "", $attribute_name);
        $wooAttrId        = $wpdb->get_var("SELECT attribute_id FROM ". $wpdb->prefix ."woocommerce_attribute_taxonomies WHERE attribute_name='{$wooAttributeName}'");

        return $wooAttrId;
    }

    /**
     * @param $term_id
     * @return null|string
     */
    public static function getTermNameById($term_id) {
        global $wpdb;
        $term_name = $wpdb->get_var("SELECT name from ". $wpdb->prefix ."terms where term_id={$term_id}");

        return $term_name;
    }

    /**
     * @param $taxonomy_id
     * @return null|string
     */
    public static function getTaxonomyNameById($taxonomy_id) {
        global $wpdb;
        $attribute_name = $wpdb->get_var("SELECT taxonomy FROM ". $wpdb->prefix ."term_taxonomy WHERE term_taxonomy_id='{$taxonomy_id}'");

        return $attribute_name;
    }

    /**
     * @param $name
     * @return null|string
     */
    public static function getTermIdByName($name) {
        global $wpdb;
        $term_id = $wpdb->get_var("SELECT term_id from ". $wpdb->prefix ."terms where `name`='{$name}'");

        return $term_id;
    }

    /**
     * @param $post
     * @param $get
     * @return null
     */
    public static function validateRequest($post, $get) {
        $postData = null;
        if (!empty($post))
        {
            $postData = $post;
        }
        else
        {
            if (!empty($get))
            {
                $postData = $get;
            }
        }
        return $postData;
    }
}