<?php
/**
 * This class controls the wp_woocommerce_parent_objects table.
 *
 * We use this table to save all the IDs from products, terms, attributes categories and tags which were inserted
 * from the parent webshop. Why? Attributes, terms, categories and tags cannot be modified or deleted when they came
 * from the parent website. Products have only a limited amount of attributes which can be edited.
 *
 * So based on whether the ID exists in this db table or not we show a different edit screen, row options or nothing at all.
 *
 * @Author: Berend de Groot
 * @Date: 14-8-17
 * @Time: 16:06
 */
class WoocommerceParentObject {
    protected static $instance;

    /**
     * @return mixed
     */
    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;

        return self::$instance;
    }

    /**
     * @param $object_id
     * @return false|int
     */
    public static function saveParentObject($object_id) {
        global $wpdb;
        $return = $wpdb->insert("". $wpdb->prefix ."woocommerce_parent_objects", array("object_id" => $object_id));

        return $return;
    }

    /**
     * @param $object_id
     */
    public static function deleteParentObject($object_id) {
        global $wpdb;

        $wpdb->delete("". $wpdb->prefix ."woocommerce_parent_objects", array("object_id" => $object_id));

    }

    /**
     * Checks whether or not a product/attribute or term is pushed from the parent webshop.
     * @param $object_id
     * @return bool
     */
    public static function isFromParent($object_id) {
        global $wpdb;

        $rowId = $wpdb->get_var($wpdb->prepare("SELECT ID from ". $wpdb->prefix ."woocommerce_parent_objects where `object_id`=%d", $object_id));

        if($rowId != false && $rowId != null) {
            return true;
        }

        return false;
    }
}