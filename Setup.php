<?php
/**
 * Setup Class, different parts of our plug-in will be loaded here.
 *
 */
defined('ABSPATH') OR exit;

class Setup {
    protected static $instance;

    /**
     * @return mixed
     */
    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;

        return self::$instance;
    }

    /**
     * Setup constructor.
     */
    public function __construct() {
        WoocommerceProduct::init();
        WoocommerceAttribute::init();
        WoocommerceTerm::init();
        WoocommerceProductPage::init();
        Settings::init();
        ParentApi::init();
        WooCommerceOrder::init();
    }

    /**
     * When the plugin is activated we need to create a database table to keep track of objects received from the parent
     * webshop.
     */
    public static function activate() {
        if (!current_user_can('activate_plugins'))
        {
            wp_die( __("You are not allowed to activate plugins", "comc") );
        }
        if (!self::checkRequiredPlugins())
        {
            wp_die( __("Please check if you have WooCommerce enabled and the cURL extension for PHP installed.", "comc") );
        }
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table1          = "
CREATE TABLE IF NOT EXISTS `". $wpdb->prefix ."woocommerce_parent_objects` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) {$charset_collate};";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($table1);
    }

    /**
     * Checks whether or not WooCommerce is installed and curl is installed.
     * @return bool
     */
    private static function checkRequiredPlugins() {
        if (!is_plugin_active("woocommerce/woocommerce.php"))
        {
            return false;
        }
        if (!function_exists("curl_version"))
        {
            return false;
        }

        return true;
    }
}