<?php

/**
 * @Author: ComSi
 * @Date: 9-11-17
 * @Time: 16:03
 */
class WooCommerceOrder {
	protected static $instance;

	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	public function __construct() {
		add_action( "woocommerce_reduce_order_stock", array( "WooCommerceOrder", "reduceStock" ) );

	}

	public static function reduceStock( $order ) {
		/** @var $order WC_Order */
		$items = $order->get_items();
		foreach ( $items as $item ) {
			/** @var  $item WC_Order_Item_Product */
			$productId = $item->get_product_id();
			if ( WoocommerceParentObject::isFromParent( $productId ) ) {
				$quantity  = $item->get_quantity();
				$wooClient = new \Automattic\WooCommerce\Client( get_option( "WebshopUrl" ), get_option( "ConsumerKey" ), get_option( "ConsumerSecret" ), array(
					"wp_api"            => true,
					"version"           => "wc/v2",
					"query_string_auth" => true
				) );
				$wooClient->put( "products/{$productId}/{$quantity}/reducestock", array( "site" => get_site_url() ) );
			}
		}
	}
}