<?php
/**
 * Makes changed to the Products page screen from WooCommerce
 *
 * - Adds a new field to the table which shows whether or not it is a shared product.
 */
defined('ABSPATH') OR exit;

class WoocommerceProductPage {
    protected static $instance;

    /**
     * @return mixed
     */
    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;

        return self::$instance;
    }

    /**
     * WoocommerceProductPage constructor.
     */
    public function __construct() {
        add_filter("post_row_actions", array("WoocommerceProductPage", "changeRowActions"), 100, 2);
        add_filter("manage_edit-product_columns", array("WoocommerceProductPage", "addFieldsProductTable"), 100, 1);
        add_action("manage_product_posts_custom_column", array("WoocommerceProductPage",
                                                               "populateSharedProductField"), 10, 2);
    }

    /**
     * @param $column_name
     * @param $post_id
     */
    public static function populateSharedProductField($column_name, $post_id) {
        if($column_name == "sharedproduct") {
            if(WoocommerceParentObject::isFromParent($post_id)) {
                echo '<span class="dashicons dashicons-yes" title="' . __('Shared product', 'comc') . '" style="cursor: help;"></span>';
            }
        }
    }

    /**
     * Add a column in the Products overview to indicate if the product is shared through the parent
     * We also make sure we have the date column as the last one by unsetting and re-adding it after
     * our newly added shared product column
     * @param $columns
     * @return mixed
     */
    public static function addFieldsProductTable($columns) {
        unset($columns["date"]);
        $columns['sharedproduct'] = '<span class="dashicons dashicons-networking parent-tips" data-tip="Shared through parent"></span>';
        $columns["date"] = __("Date", "comc");

        return $columns;
    }

    /**
     * When hovering a product in the Products overview, hide some options for this child
     * because it should only be managed through the parent-plugin
     * @param $actions
     * @param $post
     * @return mixed
     */
    public static function changeRowActions($actions, $post) {
        if(WoocommerceParentObject::isFromParent($post->ID)) {
            unset($actions["trash"]);
            unset($actions["inline hide-if-no-js"]);
            unset($actions["duplicate"]);
        }

        return $actions;
    }
}