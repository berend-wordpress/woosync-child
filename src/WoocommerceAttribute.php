<?php
/**
 * This class handles everything related to attributes.
 */
defined('ABSPATH') OR exit;

class WoocommerceAttribute {
    const ATTR_TYPE = "attribute";
    protected static $instance;

    /**
     * @return mixed
     */
    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;
        return self::$instance;
    }

    /**
     * WoocommerceAttribute constructor.
     */
    public function __construct() {
        add_action("woocommerce_attribute_added", array("WoocommerceAttribute", "saveAttribute"), 10, 2);
        add_action("woocommerce_attribute_deleted", array("WoocommerceAttribute", "deleteAttribute"), 10, 3);
    }

    /**
     * @param $insert_id
     * @param $attribute
     */
    public static function saveAttribute($insert_id, $attribute) {
            WoocommerceParentObject::saveParentObject($insert_id);
    }

    /**
     * @param      $attribute_id
     * @param null $attribute_name
     * @param null $taxonomy
     */
    public static function deleteAttribute($attribute_id, $attribute_name = null , $taxonomy = null) {
        if(WoocommerceParentObject::isFromParent($attribute_id))
        {
            WoocommerceParentObject::deleteParentObject($attribute_id);
        }
    }
}