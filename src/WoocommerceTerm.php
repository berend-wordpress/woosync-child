<?php
/**
 * This class manages the terms on a child webshop. Terms are:
 * - Categories
 * - Tags
 * - Attribute Terms
 *
 * All actions are disabled for terms pulled from the parent webshop, same as products there is limited control on
 * what can be changed. In the case of terms nothing can be changed.
 */
defined('ABSPATH') OR exit;

class WoocommerceTerm {
    const TERM_TYPE = "term";
    protected static $instance;

    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;

        return self::$instance;
    }

    public function __construct() {
        add_action("create_term", array("WoocommerceTerm", "saveTerm"), 10, 3);
        add_action("edit_terms", array("WoocommerceTerm", "updateTerm"), 10, 4);
        add_action("delete_term_taxonomy", array("WoocommerceTerm", "deleteTerm"), 10, 2);
        add_action("product_cat_row_actions", array("WoocommerceTerm", "changeRowActions"), 10, 2);
        add_action("product_tag_row_actions", array("WoocommerceTerm", "changeRowActions"), 10, 2);
    }

    /**
     * Remove actions if the term is pulled from the parent webshop.
     * @param $actions
     * @param $post
     * @return mixed
     */
    public static function changeRowActions($actions, $post) {
        if (WoocommerceParentObject::isFromParent($post->term_id))
        {
            unset($actions["edit"]);
            unset($actions["delete"]);
            unset($actions["inline hide-if-no-js"]);
            unset($actions["view"]);
        }

        return $actions;
    }

    /**
     * If a term is being updated and it is pulled from the parent webshop, we deny access.
     * @param $term_id
     */
    public static function updateTerm($term_id) {
        if (!isset($_GET["oauth_nonce"]))
        {
            if (WoocommerceParentObject::isFromParent($term_id))
            {
                wp_die( __("You can't change terms from the parent website.", "comc") );
                exit;
            }
        }
    }

    /**
     * Before the term is deleted we check if it is comming from the parent webshop, if so we deny access.
     * @param $term_id
     */
    public static function deleteTerm($term_id) {
        if (isset($_GET["oauth_nonce"]))
        {
            WoocommerceParentObject::deleteParentObject($term_id);
        }
        else
        {
            if (WoocommerceParentObject::isFromParent($term_id))
            {
                wp_die( __("You can't delete terms from the parent website.", "comc") );
                exit;
            }
        }
    }

    /**
     * @param $term_id
     */
    public static function saveTerm($term_id, $tt_id) {
        if (isset($_GET["oauth_nonce"]))
        {
            WoocommerceParentObject::saveParentObject($term_id);
            WoocommerceParentObject::saveParentObject($tt_id);
        }
    }
}