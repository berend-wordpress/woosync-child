<?php
/**
 * This class handles everything related to products. For the most part not so interesting, it just denies
 * some actions to shared productsand makes sures the parent_object table keeps properly synced.
 */
defined('ABSPATH') OR exit;
use Automattic\WooCommerce\Client;

class WoocommerceProduct {
    protected static $instance;

    /**
     * @return mixed
     */
    public static function init() {
        is_null(self::$instance) AND self::$instance == new self;

        return self::$instance;
    }

    /**
     * WoocommerceProduct constructor.
     */
    public function __construct() {
        add_action("save_post_product", array("WoocommerceProduct", "saveProduct"));
        add_action("current_screen", array("WoocommerceProduct", "enqueueScript"), 10 ,1);
        add_action("wp_trash_post", array("WoocommerceProduct", "trashProduct"));
        add_action("before_delete_post", array("WoocommerceProduct", "deleteProduct"), 10);
    }

    /**
     * @param $screen
     */
    public static function enqueueScript($screen) {
        if(isset($_GET["post"]))
        {
            $postId = $_GET["post"];
            if (WoocommerceParentObject::isFromParent($postId))
            {
                wp_enqueue_script("editProductExtra", "/wp-content/plugins/woosync-child/js/editProduct.js");
            }
        }
    }



    public static function deleteProduct($id) {
        if (WoocommerceParentObject::isFromParent($id))
        {
            if(isset($_GET["oauth_nonce"]))
            {
                WoocommerceParentObject::deleteParentObject($id);
            } else {
                wp_die( __("You can't delete products from the parent website.", "comc") );
            }
        }
    }

    public static function trashProduct($id) {
        if (WoocommerceParentObject::isFromParent($id))
        {
            if(!isset($_GET["oauth_nonce"]))
            {
                wp_die( __("You can't trash products from the parent website.", "comc") );
            }
        }
    }

    /**
     * We check if oauth_nonce isset in the request, because if so the product is being inserted from the parent.
     * If the product is inserted from the parent we only have to save the ID in the database so we know this product
     * is comming from a parent webshop.
     *
     * Whenever a product is changed/saved we check if the product is comming from the parent website, if so we send
     * a request to the parent website with the information which has been changed. De parent website will save these
     * fields so they will not be overwritten over time.
     *
     * If fields are changed which are not allowed to be changed they will be reverted immediately.
     * @param      $id
     * @param null $postData
     */
    public static function saveProduct($id, $postData = null) {
        $post = get_post($id);
        if (!isset($_GET["oauth_nonce"]))
        {
            if ($post->post_status == "publish")
            {
                if (WoocommerceParentObject::isFromParent($id))
                {
                    $exportProduct = null;
                    if ($postData == null)
                    {
                        $postData = Utility::validateRequest($_POST, $_GET);
                    }
                    $product       = wc_get_product($post->ID);
                    $productData   = $product->get_data();
                    $exportProduct = array('description'       => $productData["description"],
                                           'short_description' => $productData["short_description"]);
                    if ((isset($postData["save"]) && $postData["save"] == "Update") || $postData["bulk_edit"] == "Update")
                    {
                        if ($postData["product-type"] == "variable")
                        {
                            self::processVariations($postData, $post);
                        }
                    }
                    $wooClient                = new Client(get_option("WebshopUrl"), get_option("ConsumerKey"), get_option("ConsumerSecret"), array("wp_api"            => true,
                                                                                                                                                    "version"           => "wc/v2",
                                                                                                                                                    "query_string_auth" => true));
                    $exportProduct["siteUrl"] = get_site_url();
                    $wooClient->put("products/{$id}/fields", $exportProduct);
                }
            }
        } else {
            WoocommerceParentObject::saveParentObject($id);
        }

    }

    /**
     * @param $postData
     * @param $post
     */
    public static function processVariations($postData, $post) {
        foreach ($postData["variable_post_id"] as $varIndex => $varId)
        {
            $varProduct       = wc_get_product($varId);
            $varProductData   = $varProduct->get_data();
            $varExportProduct = null;
            foreach ($varProductData["attributes"] as $attribute => $term)
            {
                $parentAttrId                              = Utility::getWooAttrIdByName($attribute);
                $varExportProduct["attributes"][$varIndex] = array("id" => $parentAttrId, "option" => $term);
                if ($postData["variable_description"][$varIndex] != "")
                {
                    $varExportProduct["description"] = $postData["variable_description"][$varIndex];
                }
            }
        }
    }
}
