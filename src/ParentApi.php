<?php

/**
 * @Author: ComSi
 * @Date: 9-11-17
 * @Time: 16:26
 */
defined( 'ABSPATH' ) OR exit;
class ParentApi {
	protected static $instance;
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	public function __construct() {
		add_action( 'rest_api_init', function() {
			register_rest_route( "wc/v2", "/products/(?P<product_id>\d+)/(?P<reduce>\d+)/reducestock", array(
				'methods'  => 'PUT',
				'callback' => array( "ParentApi", "updateStock" ),
			) );
		} );
	}

	public static function updateStock($data) {
		/** @var $data WP_REST_Request */
		$params = $data->get_params();

		/** @var  $product WC_Product */
		$product = wc_get_product($params["product_id"]);

		$reduceStockBy = $params["reduce"];

		wc_update_product_stock($product, $reduceStockBy, "decrease");

		return "done!";
	}
}